package com.prisyazhnuy.bikeapp.repositories;

import com.google.firebase.auth.FirebaseUser;

import io.reactivex.Observable;

/**
 * Dell on 19.11.2017.
 */

public interface AuthenticationRepository {
    Observable<FirebaseUser> getCurrentUser();
    Observable<FirebaseUser> getCurrentUser(String login, String password);
    Observable<FirebaseUser> signUpUser(String login, String password);
    void logOut();
}
