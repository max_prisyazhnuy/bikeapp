package com.prisyazhnuy.bikeapp.repositories;

import com.google.android.gms.maps.model.LatLng;
import com.prisyazhnuy.bikeapp.models.Route;

import java.util.List;

import io.reactivex.Observable;

/**
 * Dell on 29.11.2017.
 */

public interface MapRestClient {
    Observable<List<Route>> getDirection(LatLng origin, LatLng destination);
}
