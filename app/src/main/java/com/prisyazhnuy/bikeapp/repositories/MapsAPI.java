package com.prisyazhnuy.bikeapp.repositories;

import com.google.android.gms.maps.model.LatLng;
import com.prisyazhnuy.bikeapp.models.DirectionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Dell on 29.11.2017.
 */

public interface MapsAPI {

    @GET("directions/json")
    Call<DirectionResponse> loadDirections(@Query("origin") String origin,
                                           @Query("destination") String destination,
                                           @Query("key") String key);
}
