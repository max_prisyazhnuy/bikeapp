package com.prisyazhnuy.bikeapp.repositories;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prisyazhnuy.bikeapp.BuildConfig;
import com.prisyazhnuy.bikeapp.models.DirectionResponse;
import com.prisyazhnuy.bikeapp.models.Route;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dell on 29.11.2017.
 */

public class MapRestClientRetrofit implements MapRestClient {

    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/";
    private MapsAPI mapsAPI;

    public MapRestClientRetrofit() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mapsAPI = retrofit.create(MapsAPI.class);

    }
    @Override
    public Observable<List<Route>> getDirection(final LatLng origin, final LatLng destination) {
        return Observable.create(new ObservableOnSubscribe<List<Route>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Route>> e) throws Exception {
                String originPoint = "" + origin.latitude + "," + origin.longitude;
                String destinationPoint = "" + destination.latitude + "," + destination.longitude;
                Call<DirectionResponse> call = mapsAPI.loadDirections(originPoint, destinationPoint, BuildConfig.MapApiKey);
                call.enqueue(new Callback<DirectionResponse>() {
                    @Override
                    public void onResponse(Call<DirectionResponse> call, Response<DirectionResponse> response) {
                        e.onNext(response.body().getRoutes());
                        e.onComplete();
                    }

                    @Override
                    public void onFailure(Call<DirectionResponse> call, Throwable t) {
                        e.onError(t);
                    }
                });
            }
        });
    }
}
