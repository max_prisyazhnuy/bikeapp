package com.prisyazhnuy.bikeapp.repositories;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Dell on 19.11.2017.
 */

public class AuthenticationRepositoryFirebase implements AuthenticationRepository {

    private FirebaseAuth mAuth;

    public AuthenticationRepositoryFirebase() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public Observable<FirebaseUser> getCurrentUser() {
        return Observable.create(new ObservableOnSubscribe<FirebaseUser>() {
            @Override
            public void subscribe(final ObservableEmitter<FirebaseUser> e) throws Exception {
                FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user == null) {
                            e.onError(new Throwable("No user"));
                        } else {
                            e.onNext(user);
                            e.onComplete();
                        }
                        mAuth.removeAuthStateListener(this);
                    }
                };
                mAuth.addAuthStateListener(authStateListener);
            }
        });
    }

    @Override
    public Observable<FirebaseUser> getCurrentUser(final String login, final String password) {
        return Observable.create(new ObservableOnSubscribe<FirebaseUser>() {
            @Override
            public void subscribe(final ObservableEmitter<FirebaseUser> e) {
                mAuth.signInWithEmailAndPassword(login, password)
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception ex) {
                                e.onError(ex);
                            }
                        })
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                e.onNext(authResult.getUser());
                                e.onComplete();
                            }
                        });
            }
        });
    }

    @Override
    public Observable<FirebaseUser> signUpUser(final String login, final String password) {
        return Observable.create(
                new ObservableOnSubscribe<FirebaseUser>() {
                    @Override
                    public void subscribe(final ObservableEmitter<FirebaseUser> e) throws Exception {
                        mAuth.createUserWithEmailAndPassword(login, password)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception ex) {
                                        e.onError(ex);
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        e.onNext(authResult.getUser());
                                        e.onComplete();
                                    }
                                });
                    }
                }
        );
    }

    @Override
    public void logOut() {
        mAuth.signOut();
    }
}
