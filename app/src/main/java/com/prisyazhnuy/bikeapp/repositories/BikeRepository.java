package com.prisyazhnuy.bikeapp.repositories;

import com.prisyazhnuy.bikeapp.models.Bike;

import java.util.List;

import io.reactivex.Observable;

/**
 * Dell on 19.11.2017.
 */

public interface BikeRepository {
    Observable<List<Bike>> getBikes();
    void addBike(Bike bike);
}
