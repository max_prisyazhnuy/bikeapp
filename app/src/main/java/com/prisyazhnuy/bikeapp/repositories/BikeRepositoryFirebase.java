package com.prisyazhnuy.bikeapp.repositories;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.prisyazhnuy.bikeapp.models.Bike;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Dell on 19.11.2017.
 */

public class BikeRepositoryFirebase implements BikeRepository {

    private DatabaseReference myRef;

    public BikeRepositoryFirebase(String uid) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference(uid);
    }

    @Override
    public Observable<List<Bike>> getBikes() {
        // Read from the database
        return Observable.create(new ObservableOnSubscribe<List<Bike>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Bike>> e) throws Exception {
                myRef.child("Bikes").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        List<Bike> bikes = new ArrayList<>();
                        for (DataSnapshot child: dataSnapshot.getChildren()) {
                            bikes.add(child.getValue(Bike.class));
                        }
                        e.onNext(bikes);
//                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        if (!e.isDisposed()) {
                            e.onError(error.toException());
                        }
                    }
                });
            }
        });
    }

    @Override
    public void addBike(Bike bike) {
        //TODO add observable for successful storing
        String key = myRef.child("Bikes").push().getKey();
        myRef.child("Bikes").child(key).setValue(bike);
    }
}
