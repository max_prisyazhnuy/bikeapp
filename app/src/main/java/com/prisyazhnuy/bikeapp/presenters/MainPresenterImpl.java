package com.prisyazhnuy.bikeapp.presenters;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.repositories.AuthenticationRepository;
import com.prisyazhnuy.bikeapp.repositories.AuthenticationRepositoryFirebase;
import com.prisyazhnuy.bikeapp.repositories.BikeRepository;
import com.prisyazhnuy.bikeapp.repositories.BikeRepositoryFirebase;
import com.prisyazhnuy.bikeapp.ui.MainView;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Dell on 19.11.2017.
 */

public class MainPresenterImpl extends MvpBasePresenter<MainView> implements MainPresenter {

    private AuthenticationRepository authenticationRepository;
    private BikeRepository bikeRepository;
    private CompositeDisposable disposable = new CompositeDisposable();

    public MainPresenterImpl(String uid) {
        authenticationRepository = new AuthenticationRepositoryFirebase();
        bikeRepository = new BikeRepositoryFirebase(uid);
    }


    @Override
    public void logOut() {
        disposable.dispose();//clear();
        authenticationRepository.logOut();
        getView().logOut();
    }

    @Override
    public void loadBikes() {
        disposable.add(bikeRepository.getBikes()
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Bike>>() {
                               @Override
                               public void accept(List<Bike> bikes) throws Exception {
                                   getView().showBikes(bikes);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                getView().errorLoadingBikes(throwable);
                            }
                        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        disposable.clear();
        super.detachView(retainInstance);
    }
}
