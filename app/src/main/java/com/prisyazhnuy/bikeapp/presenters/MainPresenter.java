package com.prisyazhnuy.bikeapp.presenters;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.prisyazhnuy.bikeapp.ui.MainView;

/**
 * Dell on 19.11.2017.
 */

public interface MainPresenter extends MvpPresenter<MainView> {
    void logOut();
    void loadBikes();
}
