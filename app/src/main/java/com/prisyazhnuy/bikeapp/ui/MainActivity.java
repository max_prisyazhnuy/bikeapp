package com.prisyazhnuy.bikeapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.presenters.MainPresenter;
import com.prisyazhnuy.bikeapp.presenters.MainPresenterImpl;
import com.prisyazhnuy.bikeapp.ui.authentication.AuthenticationActivity;
import com.prisyazhnuy.bikeapp.ui.bike.BikeFragment;
import com.prisyazhnuy.bikeapp.ui.bike_details.BikeDetailsFragment;
import com.prisyazhnuy.bikeapp.ui.bike_list.BikeListFragment;
import com.prisyazhnuy.bikeapp.ui.maps.MapsFragment;

import java.util.List;

public class MainActivity extends MvpActivity<MainView, MainPresenter> implements
        MainView, NavigationView.OnNavigationItemSelectedListener, BikeFragment.OnBikeInteractionListener,
        BikeListFragment.OnListFragmentInteractionListener {

    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        String userEmail = getIntent().getStringExtra("user_email");
        TextView tvUserEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvUserEmail);
        tvUserEmail.setText(userEmail);

        userId = getIntent().getStringExtra("user_id");


//        getPresenter().loadBikes();

    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        String userId = getIntent().getStringExtra("user_id");
        return new MainPresenterImpl(userId);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragmentById = fragmentManager.findFragmentById(R.id.container);
            if (fragmentById == null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = MapsFragment.newInstance();

                fragmentTransaction.add(R.id.container, fragment);
                fragmentTransaction.commit();
            } else {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = MapsFragment.newInstance();

                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }
        } else if (id == R.id.nav_my_bikes) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragmentById = fragmentManager.findFragmentById(R.id.container);
            if (fragmentById == null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = BikeListFragment.newInstance(userId);

                fragmentTransaction.add(R.id.container, fragment);
                fragmentTransaction.commit();
            }else {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = BikeListFragment.newInstance(userId);

                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }


        } else if (id == R.id.nav_add_bike) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragmentById = fragmentManager.findFragmentById(R.id.container);
            if (fragmentById == null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = BikeFragment.newInstance(userId);

                fragmentTransaction.add(R.id.container, fragment);
                fragmentTransaction.commit();
            } else {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = BikeFragment.newInstance(userId);

                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }

        } else if (id == R.id.nav_logout) {
            getPresenter().logOut();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void logOut() {
        Intent authenticationActivity = new Intent(this, AuthenticationActivity.class);
        startActivity(authenticationActivity);
        finish();
    }

    @Override
    public void showBikes(List<Bike> bikes) {
        Toast.makeText(this, "Bikes: " + bikes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void errorLoadingBikes(Throwable throwable) {
        Toast.makeText(this, "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBikeAdded(Bike bike) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = BikeListFragment.newInstance(userId);

        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onListFragmentInteraction(Bike item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.container);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = BikeDetailsFragment.newInstance(item);
        if (currentFragment == null) {
            fragmentTransaction.add(R.id.container, fragment);
        } else {
            fragmentTransaction.replace(R.id.container, fragment);
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
