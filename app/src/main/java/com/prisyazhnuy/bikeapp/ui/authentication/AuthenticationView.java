package com.prisyazhnuy.bikeapp.ui.authentication;

import com.google.firebase.auth.FirebaseUser;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Dell on 19.11.2017.
 */

public interface AuthenticationView extends MvpView {

    void showLoginPage();

    void authoriseUser(FirebaseUser firebaseUser);

    void userDisabled();

    void emailAlreadyInUse();

    void wrongPassword();

    void invalidEmail();

    void userNotFound();
}
