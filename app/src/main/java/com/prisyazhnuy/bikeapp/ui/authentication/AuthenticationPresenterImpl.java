package com.prisyazhnuy.bikeapp.ui.authentication;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.prisyazhnuy.bikeapp.repositories.AuthenticationRepository;
import com.prisyazhnuy.bikeapp.repositories.AuthenticationRepositoryFirebase;
import com.prisyazhnuy.bikeapp.ui.authentication.AuthenticationPresenter;
import com.prisyazhnuy.bikeapp.ui.authentication.AuthenticationView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Dell on 19.11.2017.
 */

public class AuthenticationPresenterImpl extends MvpBasePresenter<AuthenticationView> implements AuthenticationPresenter {

    private AuthenticationRepository authenticationRepository;
    private CompositeDisposable disposable = new CompositeDisposable();

    public AuthenticationPresenterImpl() {
        authenticationRepository = new AuthenticationRepositoryFirebase();
    }

    @Override
    public void autoAuthorise() {
        disposable.add(authenticationRepository.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<FirebaseUser>() {
                    @Override
                    public void accept(FirebaseUser firebaseUser) throws Exception {
                        getView().authoriseUser(firebaseUser);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        
                    }
                }));
    }

    @Override
    public void login(String login, String password) {
        disposable.add(authenticationRepository.getCurrentUser(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<FirebaseUser>() {
                               @Override
                               public void accept(FirebaseUser firebaseUser) throws Exception {
                                   getView().authoriseUser(firebaseUser);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (throwable instanceof FirebaseAuthException) {
                                    FirebaseAuthException ex = (FirebaseAuthException) throwable;
                                    switch (ex.getErrorCode()) {
                                        case "ERROR_USER_NOT_FOUND":
                                            getView().userNotFound();
                                            break;
                                        case "ERROR_WEAK_PASSWORD":
                                            break;
                                        case "ERROR_USER_DISABLED":
                                            getView().userDisabled();
                                            break;
                                        case "ERROR_EMAIL_ALREADY_IN_USE":
                                            getView().emailAlreadyInUse();
                                            break;
                                        case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                                            break;
                                        case "ERROR_USER_MISMATCH":
                                            break;
                                        case "ERROR_WRONG_PASSWORD":
                                            getView().wrongPassword();
                                            break;
                                        case "ERROR_INVALID_CREDENTIAL":
                                            break;
                                        case "ERROR_INVALID_EMAIL":
                                            getView().invalidEmail();
                                            break;
                                        default:
                                            getView().showLoginPage();
                                    }
                                } else {
                                    getView().showLoginPage();
                                }
                            }
                        }));
    }

    @Override
    public void registration(String login, String password) {
        disposable.add(authenticationRepository.signUpUser(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<FirebaseUser>() {
                               @Override
                               public void accept(FirebaseUser firebaseUser) throws Exception {
                                   getView().authoriseUser(firebaseUser);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (throwable instanceof FirebaseAuthException) {
                                    FirebaseAuthException ex = (FirebaseAuthException) throwable;
                                    switch (ex.getErrorCode()) {
                                        case "ERROR_USER_NOT_FOUND":
                                            getView().userNotFound();
                                            break;
                                        case "ERROR_WEAK_PASSWORD":
                                            break;
                                        case "ERROR_USER_DISABLED":
                                            getView().userDisabled();
                                            break;
                                        case "ERROR_EMAIL_ALREADY_IN_USE":
                                            getView().emailAlreadyInUse();
                                            break;
                                        case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                                            break;
                                        case "ERROR_USER_MISMATCH":
                                            break;
                                        case "ERROR_WRONG_PASSWORD":
                                            getView().wrongPassword();
                                            break;
                                        case "ERROR_INVALID_CREDENTIAL":
                                            break;
                                        case "ERROR_INVALID_EMAIL":
                                            getView().invalidEmail();
                                            break;
                                        default:
                                            getView().showLoginPage();
                                    }
                                } else {
                                    getView().showLoginPage();
                                }
                            }
                        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        disposable.clear();
        super.detachView(retainInstance);
    }

    /*
    ("ERROR_INVALID_CUSTOM_TOKEN", "The custom token format is incorrect. Please check the documentation."));
    ("ERROR_CUSTOM_TOKEN_MISMATCH", "The custom token corresponds to a different audience."));
    ("ERROR_INVALID_CREDENTIAL", "The supplied auth credential is malformed or has expired."));
    ("ERROR_INVALID_EMAIL", "The email address is badly formatted."));
    ("ERROR_WRONG_PASSWORD", "The password is invalid or the user does not have a password."));
    ("ERROR_USER_MISMATCH", "The supplied credentials do not correspond to the previously signed in user."));
    ("ERROR_REQUIRES_RECENT_LOGIN", "This operation is sensitive and requires recent authentication. Log in again before retrying this request."));
    ("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL", "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address."));
    ("ERROR_EMAIL_ALREADY_IN_USE", "The email address is already in use by another account."));
    ("ERROR_CREDENTIAL_ALREADY_IN_USE", "This credential is already associated with a different user account."));
    ("ERROR_USER_DISABLED", "The user account has been disabled by an administrator."));
    ("ERROR_USER_TOKEN_EXPIRED", "The user\'s credential is no longer valid. The user must sign in again."));
    ("ERROR_USER_NOT_FOUND", "There is no user record corresponding to this identifier. The user may have been deleted."));
    ("ERROR_INVALID_USER_TOKEN", "The user\'s credential is no longer valid. The user must sign in again."));
    ("ERROR_OPERATION_NOT_ALLOWED", "This operation is not allowed. You must enable this service in the console."));
    ("ERROR_WEAK_PASSWORD", "The given password is invalid."));
                        * */
}
