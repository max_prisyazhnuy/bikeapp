package com.prisyazhnuy.bikeapp.ui.bike;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.models.Bike;

public class BikeFragment extends MvpFragment<BikeView, BikePresenter> implements BikeView {

    private static final String ARG_UID = "uid";
    private OnBikeInteractionListener mListener;
    private String mUid;

    public BikeFragment() {
    }

    @NonNull
    @Override
    public BikePresenter createPresenter() {
        return new BikePresenterImpl(mUid);
    }

    public static BikeFragment newInstance(String uid) {
        BikeFragment fragment = new BikeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_UID, uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUid = getArguments().getString(ARG_UID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bike, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final EditText etName = (EditText) view.findViewById(R.id.etName);
        final EditText etModel = (EditText) view.findViewById(R.id.etModel);
        final RadioGroup rgWheelSize = (RadioGroup) view.findViewById(R.id.rgWheelSize);
        initWheelSizes(rgWheelSize);

        final RadioGroup rgBikeType = (RadioGroup) view.findViewById(R.id.rgBikeType);
        initBikeTypes(rgBikeType);
        final EditText etDistance = (EditText) view.findViewById(R.id.etDistance);

        Button btnAdd = (Button) view.findViewById(R.id.btnAddBike);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();
                String model = etModel.getText().toString();
                int wheelSize = rgWheelSize.getCheckedRadioButtonId();
                int type = rgBikeType.getCheckedRadioButtonId();
                float distance;
                try {
                    distance = Float.valueOf(etDistance.getText().toString());
                } catch (NumberFormatException ex) {
                    distance = 0.0f;
                }
                Bike bike = new Bike(name, model, wheelSize, type);
                bike.setDistance(distance);
                getPresenter().addBike(bike);
            }
        });

    }

    private void initBikeTypes(RadioGroup radioGroup) {
        if (radioGroup != null) {
            int index = 0;
            String[] bikeTypes = getResources().getStringArray(R.array.bike_types);
            for(String bikeType : bikeTypes) {
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setText(bikeType);
                radioGroup.addView(radioButton, index++);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBikeInteractionListener) {
            mListener = (OnBikeInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showBike(Bike bike) {
        if (mListener != null) {
            mListener.onBikeAdded(bike);
        }
    }

    private void initWheelSizes(RadioGroup radioGroup) {
        if (radioGroup != null) {
            int index = 0;
            String[] wheelSizes = getResources().getStringArray(R.array.wheel_sizes);
            for(String wheelSize : wheelSizes) {
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setText(wheelSize + '"');
                radioGroup.addView(radioButton, index++);
            }
        }
    }

    public interface OnBikeInteractionListener {
        void onBikeAdded(Bike bike);
    }
}
