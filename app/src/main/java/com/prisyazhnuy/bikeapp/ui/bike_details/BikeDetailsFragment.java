package com.prisyazhnuy.bikeapp.ui.bike_details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.models.Bike;

public class BikeDetailsFragment extends Fragment {

    private static final String ARG_BIKE = "bike";

    private TextView mTvName;
    private TextView mTvModel;
    private TextView mTvDistance;

    private Bike mBike;

    public BikeDetailsFragment() {
    }

    public static BikeDetailsFragment newInstance(Bike bike) {
        BikeDetailsFragment fragment = new BikeDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_BIKE, bike);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBike = getArguments().getParcelable(ARG_BIKE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bike_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTvDistance = (TextView) view.findViewById(R.id.tvDistance);
        mTvModel = (TextView) view.findViewById(R.id.tvModel);
        mTvName = (TextView) view.findViewById(R.id.tvName);

        displayBikeDetails();
    }

    private void displayBikeDetails() {
        if (mBike != null) {
            mTvName.setText(mBike.getName());
            mTvModel.setText(mBike.getModel());
            String distance = getString(R.string.distance_km, mBike.getDistance());
            mTvDistance.setText(distance);
        }
    }

}
