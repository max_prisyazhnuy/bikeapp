package com.prisyazhnuy.bikeapp.ui.authentication;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.prisyazhnuy.bikeapp.ui.authentication.AuthenticationView;

/**
 * Dell on 19.11.2017.
 */

public interface AuthenticationPresenter extends MvpPresenter<AuthenticationView> {
    void autoAuthorise();
    void login(String login, String password);
    void registration(String login, String password);
}
