package com.prisyazhnuy.bikeapp.ui.maps;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.prisyazhnuy.bikeapp.models.Route;
import com.prisyazhnuy.bikeapp.repositories.MapRestClient;
import com.prisyazhnuy.bikeapp.repositories.MapRestClientRetrofit;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Dell on 29.11.2017.
 */

public class MapsPresenterImpl extends MvpBasePresenter<MapsView> implements MapsPresenter {

    private MapRestClient mapRestClient;
    private CompositeDisposable disposable = new CompositeDisposable();

    public MapsPresenterImpl() {
        mapRestClient = new MapRestClientRetrofit();
    }
    @Override
    public void loadRoutes(LatLng origin, LatLng destination) {
        disposable.add(mapRestClient.getDirection(origin, destination)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Route>>() {
                    @Override
                    public void accept(List<Route> routes) throws Exception {
                        Log.d("loadRoutes", routes.toString());
                        getView().showRoutes(routes);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("loadRoutes", throwable.toString());
                    }
                })
        );
    }
}
