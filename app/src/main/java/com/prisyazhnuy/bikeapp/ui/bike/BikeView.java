package com.prisyazhnuy.bikeapp.ui.bike;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.prisyazhnuy.bikeapp.models.Bike;

/**
 * Dell on 22.11.2017.
 */

public interface BikeView extends MvpView {
    void showBike(Bike bike);
}
