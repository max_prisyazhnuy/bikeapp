package com.prisyazhnuy.bikeapp.ui.bike_list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.presenters.MainPresenter;
import com.prisyazhnuy.bikeapp.presenters.MainPresenterImpl;
import com.prisyazhnuy.bikeapp.ui.MainView;

import java.util.List;

public class BikeListFragment extends MvpFragment<MainView, MainPresenter> implements MainView {

    private OnListFragmentInteractionListener mListener;
    private String mUserId;
    private RecyclerView recyclerView;

    public BikeListFragment() {
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImpl(mUserId);
    }

    @SuppressWarnings("unused")
    public static BikeListFragment newInstance(String uid) {
        BikeListFragment fragment = new BikeListFragment();
        Bundle args = new Bundle();
        args.putString("userId", uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString("userId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bike_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            recyclerView = (RecyclerView) view;
//            getPresenter().loadBikes();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPresenter().loadBikes();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void showBikes(List<Bike> bikes) {
        Context context = recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new BikeRecyclerViewAdapter(bikes, mListener));
    }

    @Override
    public void errorLoadingBikes(Throwable throwable) {

    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Bike item);
    }
}
