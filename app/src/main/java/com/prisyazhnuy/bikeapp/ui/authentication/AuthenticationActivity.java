package com.prisyazhnuy.bikeapp.ui.authentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.ui.MainActivity;

public class AuthenticationActivity extends MvpActivity<AuthenticationView, AuthenticationPresenter> implements AuthenticationView {

    private EditText mEtLogin;
    private EditText mEtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        getPresenter().autoAuthorise();

        mEtLogin = (EditText) findViewById(R.id.etLogin);
        mEtPassword = (EditText) findViewById(R.id.etPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().login(mEtLogin.getText().toString(), mEtPassword.getText().toString());
            }
        });

        Button btnRegistration = (Button) findViewById(R.id.btnRegistration);
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().registration(mEtLogin.getText().toString(), mEtPassword.getText().toString());
            }
        });
    }

    @NonNull
    @Override
    public AuthenticationPresenter createPresenter() {
        return new AuthenticationPresenterImpl();
    }

    @Override
    public void showLoginPage() {
        Toast.makeText(this, "Can not authorise", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void authoriseUser(FirebaseUser firebaseUser) {
        Intent mainActivity = new Intent(this, MainActivity.class);
        mainActivity.putExtra("user_id", firebaseUser.getUid());
        mainActivity.putExtra("user_email", firebaseUser.getEmail());
        startActivity(mainActivity);
        finish();
    }

    @Override
    public void userDisabled() {
        mEtLogin.setError(getString(R.string.user_disabled));
        mEtLogin.requestFocus();
        mEtPassword.setError(null);
    }

    @Override
    public void emailAlreadyInUse() {
        mEtLogin.setError(getString(R.string.email_is_use));
        mEtLogin.requestFocus();
        mEtPassword.setError(null);
    }

    @Override
    public void wrongPassword() {
        mEtPassword.setError(getString(R.string.wrong_password));
        mEtPassword.requestFocus();
        mEtLogin.setError(null);
    }

    @Override
    public void invalidEmail() {
        mEtLogin.setError(getString(R.string.invalid_email));
        mEtLogin.requestFocus();
        mEtPassword.setError(null);
    }

    @Override
    public void userNotFound() {
        mEtLogin.setError(getString(R.string.user_not_found));
        mEtLogin.requestFocus();
        mEtPassword.setError(null);
    }
}
