package com.prisyazhnuy.bikeapp.ui;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.prisyazhnuy.bikeapp.models.Bike;

import java.util.List;

/**
 * Dell on 19.11.2017.
 */

public interface MainView extends MvpView {
    void logOut();
    void showBikes(List<Bike> bikes);
    void errorLoadingBikes(Throwable throwable);
}
