package com.prisyazhnuy.bikeapp.ui.bike_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prisyazhnuy.bikeapp.R;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.ui.bike_list.BikeListFragment.OnListFragmentInteractionListener;

import java.util.List;

public class BikeRecyclerViewAdapter extends RecyclerView.Adapter<BikeRecyclerViewAdapter.ViewHolder> {

    private final List<Bike> mValues;
    private final OnListFragmentInteractionListener mListener;

    public BikeRecyclerViewAdapter(List<Bike> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bike_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTvName.setText(mValues.get(position).getName());
        holder.mTvModel.setText(mValues.get(position).getModel());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTvName;
        public final TextView mTvModel;
        public Bike mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTvName = (TextView) view.findViewById(R.id.tvName);
            mTvModel = (TextView) view.findViewById(R.id.tvModel);
        }
    }
}
