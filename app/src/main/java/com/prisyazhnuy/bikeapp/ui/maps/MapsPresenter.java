package com.prisyazhnuy.bikeapp.ui.maps;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * Dell on 29.11.2017.
 */

public interface MapsPresenter extends MvpPresenter<MapsView> {
    void loadRoutes(LatLng origin, LatLng destination);
}
