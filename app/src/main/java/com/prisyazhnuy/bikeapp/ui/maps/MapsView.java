package com.prisyazhnuy.bikeapp.ui.maps;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.prisyazhnuy.bikeapp.models.Route;

import java.util.List;

/**
 * Dell on 29.11.2017.
 */

public interface MapsView extends MvpView {
    void showRoutes(List<Route> routes);
}
