package com.prisyazhnuy.bikeapp.ui.bike;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.repositories.BikeRepository;
import com.prisyazhnuy.bikeapp.repositories.BikeRepositoryFirebase;

/**
 * Dell on 22.11.2017.
 */

public class BikePresenterImpl extends MvpBasePresenter<BikeView> implements BikePresenter {

    private BikeRepository bikeRepository;

    public BikePresenterImpl(String uid) {
        bikeRepository = new BikeRepositoryFirebase(uid);
    }

    @Override
    public void addBike(Bike bike) {
        bikeRepository.addBike(bike);
        getView().showBike(bike);
    }
}
