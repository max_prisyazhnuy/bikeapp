package com.prisyazhnuy.bikeapp.ui.bike;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.prisyazhnuy.bikeapp.models.Bike;
import com.prisyazhnuy.bikeapp.ui.bike.BikeView;

/**
 * Dell on 22.11.2017.
 */

public interface BikePresenter extends MvpPresenter<BikeView> {
    void addBike(Bike bike);
}
