package com.prisyazhnuy.bikeapp.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Dell on 29.11.2017.
 */

public class Leg {
    @SerializedName("start_location")
    private LatLng startLocation;
    @SerializedName("end_location")
    private LatLng endLocation;
    @SerializedName("distance")
    private Pair distance;
    @SerializedName("duration")
    private Pair duration;

    public String getEnd_address() {
        return end_address;
    }

    public void setEnd_address(String end_address) {
        this.end_address = end_address;
    }

    @SerializedName("end_address")
    private String end_address;

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public Pair getDistance() {
        return distance;
    }

    public void setDistance(Pair distance) {
        this.distance = distance;
    }

    public Pair getDuration() {
        return duration;
    }

    public void setDuration(Pair duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Leg{" +
                "startLocation=" + startLocation +
                ", endLocation=" + endLocation +
                ", distance=" + distance +
                ", duration=" + duration +
                ", end_address=" + end_address +
                '}';
    }
}
