package com.prisyazhnuy.bikeapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Dell on 19.11.2017.
 */

public class Bike implements Parcelable {

    private String name;
    private String model;
    private float distance;
    private float maxSpeed;
    private int wheelSize;
    private int type;

    public Bike() {}

    public Bike(String name, String model, int wheelSize, int type) {
        this.name = name;
        this.model = model;
        this.wheelSize = wheelSize;
        this.type = type;
    }

    protected Bike(Parcel in) {
        name = in.readString();
        model = in.readString();
        distance = in.readFloat();
        maxSpeed = in.readFloat();
        wheelSize = in.readInt();
        type = in.readInt();
    }

    public static final Creator<Bike> CREATOR = new Creator<Bike>() {
        @Override
        public Bike createFromParcel(Parcel in) {
            return new Bike(in);
        }

        @Override
        public Bike[] newArray(int size) {
            return new Bike[size];
        }
    };

    public int getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(int wheelSize) {
        this.wheelSize = wheelSize;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", distance=" + distance +
                ", maxSpeed=" + maxSpeed +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(model);
        dest.writeFloat(distance);
        dest.writeFloat(maxSpeed);
        dest.writeInt(wheelSize);
        dest.writeInt(type);
    }
}
