package com.prisyazhnuy.bikeapp.models;

/**
 * Dell on 29.11.2017.
 */

public class Polyline {
    private String points;

    public Polyline() {
    }

    public Polyline(String points) {
        this.points = points;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Polyline{" +
                "points='" + points + '\'' +
                '}';
    }
}
