package com.prisyazhnuy.bikeapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Dell on 30.11.2017.
 */

public class Pair {
    @SerializedName("value")
    private double value;
    @SerializedName("text")
    private String text;

    @Override
    public String toString() {
        return "Pair{" +
                "value=" + value +
                ", text='" + text + '\'' +
                '}';
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
