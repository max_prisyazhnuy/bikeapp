package com.prisyazhnuy.bikeapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Dell on 29.11.2017.
 */

/*"routes": [ {
        "summary": "I-40 W",
        "legs": [ {
        "steps": [ {
        "travel_mode": "DRIVING",
        "start_location": {
        "lat": 41.8507300,
        "lng": -87.6512600
        },
        "end_location": {
        "lat": 41.8525800,
        "lng": -87.6514100
        },
        "polyline": {
        "points": "a~l~Fjk~uOwHJy@P"
        },
        "duration": {
        "value": 19,
        "text": "1 min"
        },
        "html_instructions": "Head \u003cb\u003enorth\u003c/b\u003e on \u003cb\u003eS Morgan St\u003c/b\u003e toward \u003cb\u003eW Cermak Rd\u003c/b\u003e",
        "distance": {
        "value": 207,
        "text": "0.1 mi"
        }
        },
        ...
        ... additional steps of this leg
        ...
        ... additional legs of this route
        "duration": {
        "value": 74384,
        "text": "20 hours 40 mins"
        },
        "distance": {
        "value": 2137146,
        "text": "1,328 mi"
        },
        "start_location": {
        "lat": 35.4675602,
        "lng": -97.5164276
        },
        "end_location": {
        "lat": 34.0522342,
        "lng": -118.2436849
        },
        "start_address": "Oklahoma City, OK, USA",
        "end_address": "Los Angeles, CA, USA"
        } ]
*/
public class Route {

    @SerializedName("summary")
    private String summary;
    @SerializedName("legs")
    private List<Leg> legs;
    @SerializedName("start_address")
    private String startAddress;
    @SerializedName("end_address")
    private String endAddress;
    @SerializedName("overview_polyline")
    private Polyline polyline;

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    @Override
    public String toString() {
        return "Route{" +
                "summary='" + summary + '\'' +
                ", legs=" + legs +
                ", startAddress='" + startAddress + '\'' +
                ", endAddress='" + endAddress + '\'' +
                ", polyline=" + polyline +
                '}';
    }
}
