package com.prisyazhnuy.bikeapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Dell on 29.11.2017.
 */

public class DirectionResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("routes")
    private List<Route> routes;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "DirectionResponse{" +
                "status='" + status + '\'' +
                ", routes=" + routes +
                '}';
    }
}
